// Menú principal
const openMenu = document.querySelector('.menu-principal__open');
const closeMenu = document.querySelector('.menu-principal__close');
const menu = document.querySelector('.menu');


openMenu.addEventListener('click', function() {
	menu.classList.add("menu__open");
});
closeMenu.addEventListener('click', function(){
	menu.classList.remove("menu__open");
});

// Submenú

const subMenu = document.querySelector('.submenu');
const openSubmenu = document.querySelector('.item-submenu');

openSubmenu.addEventListener('click', function(){
	subMenu.classList.toggle("submenu__open");
});
// Registro de usuarios
const openUser = document.querySelector('.carrito__usuario');
const formulario = document.querySelector('.usuario');
const tituloUser = document.querySelector('.usuario__titulo');

openUser.addEventListener('click', function () {
	formulario.classList.toggle("display-user");
});
