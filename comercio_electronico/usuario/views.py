from django.shortcuts import render, redirect
from django.contrib.auth import logout,login, authenticate


def cerrarSesion(request):
	logout(request)
	return redirect("index")