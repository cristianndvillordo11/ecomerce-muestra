from django import forms

from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from .models import Perfil

class NuevoUsuarioForm(UserCreationForm):
    class Meta:
        model = Perfil
        fields = ("first_name","last_name","username", "email", "password1",
         "password2","foto", "dni")

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields["first_name"].widget.attrs.update({"type":"text", 'id':"nombre", 'class':"formulario__campo","placeholder":"ej. Cristian"})
        self.fields["last_name"].widget.attrs.update({"type":"text", 'id':'apellido',"class":"formulario__campo","placeholder":"ej. Villordo"})
        self.fields["email"].widget.attrs.update({"type":"email",'id':'correo', "class":"formulario__campo", "placeholder":"email@example.com"})
        self.fields["username"].widget.attrs.update({"type":"text", "class":"formulario__campo", "placeholder" : "nombre de usuario"})
        self.fields["password1"].widget.attrs.update({"type":"password", 'id':'password',"class":"formulario__campo", "placeholder":"Ingrese su Contraseña"})
        self.fields["password2"].widget.attrs.update({"type":"password", 'id':'password',"class":"formulario__campo", "placeholder":"confirmar Contraseña"})
        self.fields["foto"].widget.attrs.update({"type":"file", "class":"formulario__campo-file"})
        self.fields["dni"].widget.attrs.update({"type":"text", 'id':'dni',"class":"formulario__campo", "placeholder":"Introduzca su DNI, Debe poner 8 números","maxlength":"8"})

