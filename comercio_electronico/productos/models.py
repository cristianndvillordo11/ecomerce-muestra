from django.db import models
from django.contrib.auth.models import AbstractUser, User
from usuario.models import Perfil

class Categorias(models.Model):
    categoria = models.CharField(max_length=64, null=False)

    def __str__(self):
        return f"{self.categoria}"


class Productos(models.Model):
	categoria = models.ForeignKey(Categorias, on_delete=models.CASCADE, related_name="clasificacion_seccion")
	fecha_creacion = models.DateTimeField(auto_now_add=True)
	titulo = models.CharField(max_length=250, null=False)
	contenido = models.CharField(max_length=2000, null=False)
	imagen = models.ImageField(upload_to="productos", null=True, blank=True)
	precio = models.CharField(max_length=10, default=0)
	precio_oferta = models.CharField(max_length=10, default=0)
	usuario = models.ForeignKey(Perfil, on_delete=models.CASCADE, related_name="usuario")
	productos_disponibles = models.BooleanField(default = True)

	def __str__(self):
		return f"{self.fecha_creacion} - {self.titulo} - ${self.precio} ({self.usuario})"


class Carritos(models.Model):
	usuario =  models.ForeignKey(Perfil, on_delete = models.SET_NULL, null = True)
	producto = models.ForeignKey(Productos, null=True, blank=True, on_delete=models.CASCADE)
	fecha_creacion = models.DateTimeField(auto_now_add=True)
	cantidad = models.CharField(default=1, max_length=3)


	def __str__(self):
		return f"{self.producto} - ({self.usuario})"