from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import logout,login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from .forms import ProductosForm, SearchForm, CarritoForm, CategoriasForm
from .models import Productos, Categorias, Carritos
from usuario.models import Perfil
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import PasswordChangeForm
from django.core.paginator import Paginator
from django.utils import timezone
from django.contrib.auth.models import  User

#@login_required
def crear_producto(request):
	#BUSQUEDA--------------------------------------------------------------------------------------------------------
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	
	#CREAR PRODUCTO---------------------------------------------------------------------------------------------------
	if request.method == "POST":
		form= ProductosForm(request.POST,request.FILES)
		if form.is_valid():
			producto=form.save(commit=False)
			producto.usuario = request.user
			producto.save()
			return redirect('index')
		else:
			contexto={"form":form,
			}
			return render(request, "productos/agregar_productos.html",contexto)
	form = ProductosForm()

	lista_productos = Productos.objects.all()[0:16]
	lista_categorias = Categorias.objects.all()

	contexto={"form":form,
			"search_form":search_form,
			"lista_categorias": lista_categorias,
			'lista_productos': lista_productos,
			}
	return render(request, "productos/agregar_productos.html",contexto)

#@login_required
def borrarProducto(request, id):
  	producto = get_object_or_404(Productos, id=id)
  	producto.delete()
  	return redirect("index")
  	
#@login_required
def editar_producto(request, id):
	#BUSQUEDA--------------------------------------------------------------------------------------------------------
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	producto = get_object_or_404(Productos, id=id)
	if request.method == "POST":
		user = Perfil.objects.get(username=request.user)
		producto.usuario = user
		form = ProductosForm(data=request.POST, files=request.FILES, instance=producto)
		if form.is_valid():
			form.save()
			return redirect("index")
	else:
		form = ProductosForm(instance = producto)
		return render(request, 'productos/editar_producto.html', {
			"producto": producto,
			"form": form,
        })

def search(request):
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()

	filtro_titulo = request.GET.get("titulo", "")
	orden_post = request.GET.get("orden", None)
	lista_productos = Productos.objects.filter(titulo__icontains = filtro_titulo)

	if orden_post == "titulo":
		lista_productos= lista_productos.order_by("titulo")
	elif orden_post == "antiguo":
		lista_productos= lista_productos.order_by("fecha_creacion")
	elif orden_post == "nuevo":
		lista_productos= lista_productos.order_by("-fecha_creacion")

	#paginate
	
	paginate = Paginator(lista_productos, 12)
	page_number = request.GET.get('page')
	page_obj = paginate.get_page(page_number)

	form = SearchForm()
	lista_categorias = Categorias.objects.all()		
	contexto = { "form":form,
				 "lista_productos":lista_productos,
				 "search_form":search_form,
				 "lista_categorias": lista_categorias,
				 "page_obj" : page_obj,
				 }
	return render(request, "productos/search.html",contexto)

def agregar_categoria(request):
	if request.method == "POST":
		form= CategoriasForm(request.POST,request.FILES)
		if form.is_valid():
			categoria=form.save(commit=False)
			categoria.usuario = request.user
			categoria.save()
		else:
			contexto={"form":form,
				#	"search_form":search_form,
			}
			return render(request, "productos/agregar_categorias.html",contexto)
	form = CategoriasForm()
	lista_categorias = Categorias.objects.all()
	categoria : Categorias.objects.all()
	contexto={"form":form,
				"lista_categorias": lista_categorias,
		#	"search_form":search_form,
			}
	return render(request, "productos/agregar_categorias.html",contexto)

def filtro_categoria(request, id):
    categoria = get_object_or_404(Categorias, id=id)
    queryset = Productos.objects.all()
    queryset = queryset.filter(categoria=categoria)
    lista_categorias = Categorias.objects.all()
    contexto = {
        "lista_productos": queryset,
        "lista_categorias": lista_categorias,
        "categoria_seleccionada": categoria,
        }
    return render(request,"index.html", contexto)

def editar_categoria(request, id):
	categoria = get_object_or_404(Categorias, id=id)
	if request.method == "POST":
		user = Perfil.objects.get(username=request.user)
		categoria.usuario = user
		form = CategoriasForm(data=request.POST, files=request.FILES, instance=categoria)
		if form.is_valid():
			form.save()
			return redirect("productos:agregar_categoria")
	else:
		form = CategoriasForm(instance = categoria)
		return render(request, 'productos/editar_categoria.html', {
            "categoria": categoria,
            "form": form
        })

def borrar_categoria(request, id):
  	categoria = get_object_or_404(Categorias, id=id)
  	categoria.delete()
  	return redirect("productos:agregar_categoria")

#@login_required
def carrito(request):
	#BUSQUEDA--------------------------------------------------------------------------------------------------------
#	if request.GET:
#		search_form = SearchForm(request.GET)
#	else:
#		search_form = SearchForm()
	
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	
	carritos = Carritos.objects.all()
	
	#total de productos
	total= 0
	total_cantidad = 0
	for x in carritos:
		total+= int(x.producto.precio_oferta)
		total*= int(x.cantidad)
		total_cantidad += int(x.cantidad)
		form = CarritoForm()


	#modificar cantidad de productos en el carrito
	lista_categorias = Categorias.objects.all()
	form = CarritoForm()
	contexto ={
#    "search_form":search_form,
	'carritos':carritos,
	
	'total':total,
	'total_cantidad':total_cantidad,
	'form_carrito':form,
	"lista_categorias": lista_categorias,
    }
	return render(request,"carritos/carritos.html", contexto)

#@login_required
def agregarCarrito(request, id):
#	if request.GET:
#		search_form = SearchForm(request.GET)
#	else:
#		search_form = SearchForm()
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	
	#agregar carrito----------------------------------------------------------------------------------------------------
	producto = Productos.objects.get(pk=id)

	if request.method == "POST":
		form = CarritoForm(request.POST)		
		user = Perfil.objects.get(username=request.user)
		if form.is_valid():
			carrito = form.save(commit=False)
			carrito.usuario = request.user
			carrito.producto = producto
			carrito.save()
			return redirect('index')

	return redirect('index')



@login_required
def borrar_carrito(request, id):
	carrito = get_object_or_404(Carritos, id=id)
	carrito.delete()
	return redirect("index")
@login_required
def editar_carrito(request, id):
    carrito = get_object_or_404(Carritos, id=id)
    if request.method == "POST":  
        user = Perfil.objects.get(username=request.user)   
        carrito.usuario = user
        form = CarritoForm(data=request.POST, files=request.FILES, instance=carrito)
        if form.is_valid():
            form.save()
            return redirect("productos:carrito")
    else:
        form = CarritoForm(instance = carrito)
        return render(request, 'carritos/carritos.html', {
            "carrito": carrito,
            "form": form
        })

def search(request):
	form = SearchForm()
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()
	# parametros
	param_titulo = request.GET.get('titulo','')
	# filtrar titulo
	lista_productos = Productos.objects.filter(titulo__contains=param_titulo)
	
	
#	forms = AuthenticationForm()
#	if request.method == "POST":
#		forms = AuthenticationForm(data=request.POST)
#		if forms.is_valid():
#			username = forms.cleaned_data["username"]
#			password = forms.cleaned_data["password"]
#			user = authenticate(username=username, password=password)
#			if user is not None:
#				login(request, user)
#				return redirect("bienvenido")
	contexto = {"form":form,
	     		"lista_productos":lista_productos,
				"search_form":search_form,
				
				 }
	return render(request, "productos/search.html",contexto)
