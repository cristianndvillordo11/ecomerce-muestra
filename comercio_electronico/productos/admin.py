from django.contrib import admin
from .models import Productos, Categorias, Carritos

admin.site.register(Productos)
admin.site.register(Categorias)
admin.site.register(Carritos)