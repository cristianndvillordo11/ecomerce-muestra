from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm

from django.contrib.auth.models import User
from .models import Productos, Carritos, Categorias

class ProductosForm(forms.ModelForm):
    class Meta:
        model = Productos
        fields = "__all__"
        exclude = ('usuario',)

    def __init__(self, *args, **kwargs):
        super(ProductosForm, self).__init__(*args, **kwargs)
        self.fields["categoria"].widget.attrs.update({'class': 'campo__input', 'type':"text", 'placeholder':"elija la categoria"})
        self.fields["titulo"].widget.attrs.update({'class': 'campo__input', 'type':"text", 'placeholder':"Nombre del producto"})
        self.fields["precio"].widget.attrs.update({'class': 'campo__input', 'type':"text", 'placeholder':"precio del producto"})
        self.fields["precio_oferta"].widget.attrs.update({'class': 'campo__input', 'type':"text",})
        self.fields["contenido"].widget.attrs.update({'class': 'campo__input', 'type':"textarea", 'placeholder':"Descripción"})
        self.fields["productos_disponibles"].widget.attrs.update({'class': 'campo_p-disponible', 'for':"disponibilidad"})
        self.fields["imagen"].widget.attrs.update({'class': 'campo_p-file', 'id':'file', "type":"file",'accept':'.jpg,.png'})
        
class CategoriasForm(forms.ModelForm):
    class Meta:
        model = Categorias
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(CategoriasForm, self).__init__(*args, **kwargs)
        self.fields["categoria"].widget.attrs.update({'class': 'campo__input', 'type':"text", 'placeholder':"Nombre de la categoria"})

class SearchForm(forms.Form):
    titulo = forms.CharField(max_length=30, required = False)
    ORDER_OPCIONES = (
        ("titulo", "Titulo"),
        ("Fecha",(
            ("antiguo", "Antiguo"),
            ("nuevo", "Nuevo"))
        ))
    orden = forms.ChoiceField(choices=ORDER_OPCIONES, required = False,
        initial="nuevo")

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields["titulo"].widget.attrs.update({"class":"buscador__campo", "type":"search", "placeholder":"Buscar", "aria-label":"Search"})

class CarritoForm(forms.ModelForm):
    class Meta:
        model = Carritos
        fields = ('cantidad',)

    def __init__(self, *args, **kwargs):
        super(CarritoForm, self).__init__(*args, **kwargs)
        self.fields["cantidad"].widget.attrs.update({'type':"number",'value':"{{'cantidad'}}",'class':"carrito__campo", 'id':"cantidad", 'name':"cantidad", 'min':'0'})