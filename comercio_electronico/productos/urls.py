from django.urls import path, include
from . import views

app_name = "productos"
urlpatterns = [
    path('filtro_categoria/<int:id>', views.filtro_categoria, name="filtro_categoria"),
    path('crear_producto/', views.crear_producto, name="crear_producto"),
    path("<int:id>/borrar", views.borrarProducto, name="borrarProducto"),
    path("editar_producto/<int:id>", views.editar_producto, name="editar_producto"),
    path('carrito', views.carrito, name="carrito"),
    path('carrito/<int:id>/agregarCarrito', views.agregarCarrito, name="agregarCarrito"),
    path("<int:id>/borrar_carrito", views.borrar_carrito, name="borrar_carrito"),
    path("<int:id>/editar_carrito", views.editar_carrito, name="editar_carrito"),
    path("search", views.search, name="search"),
    path('agregar_categoria/', views.agregar_categoria, name="agregar_categoria"),
    path("editar_categoria/<int:id>", views.editar_categoria, name="editar_categoria"),
    path("<int:id>/borrar_categoria", views.borrar_categoria, name="borrar_categoria"),
]