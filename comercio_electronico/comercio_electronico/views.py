from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from productos.models import Productos, Categorias, Carritos
from django.contrib.auth import logout,login, authenticate
from django.core.paginator import Paginator
from productos.forms import SearchForm, CarritoForm
from usuario.forms import NuevoUsuarioForm

def index(request):
	#BUSQUEDA--------------------------------------------------------------------------------------------------------
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	
	#PAGINACION-----------------------------------------------------------
	lista_productos = Productos.objects.all().order_by( "-fecha_creacion")
	paginate = Paginator(lista_productos, 4)
	page_number = request.GET.get('page')
	page_obj = paginate.get_page(page_number)
	form_carrito = CarritoForm()
	lista_productos = Productos.objects.all()[0:16]
	lista_categorias = Categorias.objects.all()
	form_search = SearchForm()
	contexto ={
		'form_carrito':form_carrito,
		'form_search':form_search,
		'search_form':search_form,	
		"page_obj" : page_obj,
		"lista_categorias": lista_categorias,
		'lista_productos': lista_productos,
		}
	return render(request,"index.html", contexto)